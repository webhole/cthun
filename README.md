# C'thun
C'thun - ansible playbooks storage
## ❗️Reminder
Do not forget to create `.av_pass` in project with vault password or change `ansible.cfg`
## Notes
#### configure_home_desktop.yaml
Allows to configure desktop from scratch

| Tag                        |          Description          |
|:---------------------------|:-----------------------------:|
| nas_mount                  | Mount shared folders from NAS |
| nas_unmount                |    Unmount shared folders     |
| configure_system           |  Apply system level settings  |

### Configure desktop machine with playbooks
